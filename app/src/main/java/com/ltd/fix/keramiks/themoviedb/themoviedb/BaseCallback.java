package com.ltd.fix.keramiks.themoviedb.themoviedb;

import com.ltd.fix.keramiks.themoviedb.network.models.Movies;
import com.ltd.fix.keramiks.themoviedb.network.models.Movie;

import java.util.List;

import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public abstract class BaseCallback implements Callback<Movies> {

    @Override
    public void onResponse(Response<Movies> response, Retrofit retrofit) {

        switch (response.code()){
            case 200:
                if(response.body() != null)
                    onSuccess(response.body().getResults());
                else onFailure(new Throwable("Sorry,server was disable"));
                break;
            case 503:
                onFailure(new Throwable("Service offline"));
                break;
            case 400:
                onFailure(new Throwable("Invalid page"));
                break;
        }

    }


    public abstract void onSuccess(List<Movie> list );

    public abstract void onFailure(Throwable t);
}

