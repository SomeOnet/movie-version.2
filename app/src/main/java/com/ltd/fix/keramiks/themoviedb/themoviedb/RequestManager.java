package com.ltd.fix.keramiks.themoviedb.themoviedb;

import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public final class RequestManager {
    private final static RequestManager INSTANCE = new RequestManager();

    private static final String API_KEY = "70975a712a50f1825d37028d9a9d58fb";

    private Service service;

    private RequestManager(){
    }

    public void initialize(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Params.BASE_URL)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        OkHttpClient client = retrofit.client();
        client.interceptors().add(new AuthInterceptor(API_KEY));

        INSTANCE.service = retrofit.create(Service.class);
    }

    public static RequestManager getInstance(){
         return INSTANCE;
    }

    public Service getService() {
        if(INSTANCE.service == null) {
            throw new IllegalStateException("RequestManager is not initialise");
        }
        return INSTANCE.service;
    }

    private static final class AuthInterceptor implements Interceptor {
        private final String API_KEY;

        AuthInterceptor(String API_KEY) {
            this.API_KEY = API_KEY;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            HttpUrl url = request.httpUrl()
                    .newBuilder()
                    .addQueryParameter(Params.PARAM_API_KEY, API_KEY)
                    .build();

            request = request.newBuilder().url(url).build();
            Response response = chain.proceed(request);
            return response;
        }

    }

}