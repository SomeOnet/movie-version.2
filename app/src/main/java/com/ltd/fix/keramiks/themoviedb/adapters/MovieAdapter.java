package com.ltd.fix.keramiks.themoviedb.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.ltd.fix.keramiks.R;
import com.ltd.fix.keramiks.themoviedb.network.models.Movie;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.CardViewHolder> {



    private ArrayList<Movie> movieArrayList;
    private LayoutInflater inflater;
    Context context;

    public MovieAdapter(Context context){
        movieArrayList = new ArrayList<>();
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.card_text)
        public TextView textView;
        @Bind(R.id.card_view_image)
        public SimpleDraweeView imageView;
        @Bind(R.id.card_date)
        public TextView date;
        @Bind(R.id.card_popularity)
        public TextView popularity;

        public CardViewHolder(final View item) {
            super(item);
            ButterKnife.bind(this, item);
        }

    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Fresco.initialize(context);
        return new CardViewHolder(inflater.inflate(R.layout.item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        Movie item = movieArrayList.get(position);
        holder.textView.setText(item.getTitle());
        holder.date.setText(item.getRelease_date());
        holder.popularity.setText(String.valueOf(item.getPopularity()));
        holder.imageView.setImageURI(Uri.withAppendedPath(item.POSTER_URL, item.getPoster_path()));
    }

    public void Pagination(@NonNull Collection collection){
        int curSize = getItemCount();
        movieArrayList.addAll(collection);
        notifyItemRangeInserted(curSize, getItemCount());
    }

    public Movie getItem(int position){
        return movieArrayList.get(position);
    }

    @Override
    public int getItemCount() {
        return movieArrayList.size();
    }

}