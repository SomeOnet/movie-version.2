package com.ltd.fix.keramiks.themoviedb;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.ltd.fix.keramiks.R;
import com.ltd.fix.keramiks.themoviedb.network.models.Movie;
import com.ltd.fix.keramiks.themoviedb.themoviedb.Parc;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Movie_Detail extends AppCompatActivity {


    Parc parc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie__detail);
        ButterKnife.bind(this);
        initActivity();
    }

    @Bind(R.id.title_Text)
    TextView textView;

    @Bind(R.id.image_view)
    SimpleDraweeView imageView;

    @Bind(R.id.text_date)
    TextView text_date;

    @Bind(R.id.text_about)
    TextView textViewAbout;

    @Bind(R.id.language)
    TextView language;

    public void initActivity() {

        Movie item = (Movie) getIntent().getParcelableExtra(parc.EXTRA_MOVIE_PARCELABLE);

        textView.setText(item.getTitle());
        text_date.setText(item.getRelease_date());
        textViewAbout.setText(item.getOverview());
        language.setText(item.getOriginal_language());
        imageView.setImageURI(Uri.withAppendedPath(item.POSTER_URL, item.getBackdrop_path()));
    }
}
