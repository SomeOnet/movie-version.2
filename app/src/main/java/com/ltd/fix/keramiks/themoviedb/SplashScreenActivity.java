package com.ltd.fix.keramiks.themoviedb;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.ltd.fix.keramiks.R;

import java.util.concurrent.TimeUnit;




public class SplashScreenActivity extends AppCompatActivity {

    private static final int SPLASH_TIME = (int) TimeUnit.SECONDS.toMillis(3);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        splash();
    }

    public void splash(){
         new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                finish();
            }
        }, SPLASH_TIME);
    }

}
