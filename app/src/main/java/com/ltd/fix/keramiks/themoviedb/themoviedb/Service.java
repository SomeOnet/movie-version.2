package com.ltd.fix.keramiks.themoviedb.themoviedb;

import com.ltd.fix.keramiks.themoviedb.network.models.Movies;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface Service {

    @GET("/3/discover/movie")
    Call<Movies> getResentMovies(@Query(Params.PARAM_PAGE) int page,
                                    @Query(Params.PARAM_RELEASE_DATE_gte) String date_gte,
                                    @Query(Params.PARAM_RELEASE_DATE_lte) String date_lte);

    @GET("/3/discover/movie")
    Call<Movies> getPopularMovies(@Query(Params.PARAM_PAGE) int page,
                                     @Query(Params.PARAM_SORT) String sort_by);

    @GET("3/search/movie")
    Call<Movies> searchByTitle(@Query("query") String query);
}