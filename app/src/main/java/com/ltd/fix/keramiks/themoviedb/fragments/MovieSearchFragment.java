package com.ltd.fix.keramiks.themoviedb.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.ltd.fix.keramiks.R;
import com.ltd.fix.keramiks.themoviedb.Movie_Detail;
import com.ltd.fix.keramiks.themoviedb.adapters.MovieAdapter;
import com.ltd.fix.keramiks.themoviedb.adapters.RecyclerItemClickListener;
import com.ltd.fix.keramiks.themoviedb.network.models.Movie;
import com.ltd.fix.keramiks.themoviedb.themoviedb.BaseCallback;
import com.ltd.fix.keramiks.themoviedb.themoviedb.Parc;
import com.ltd.fix.keramiks.themoviedb.themoviedb.RequestManager;
import com.ltd.fix.keramiks.themoviedb.themoviedb.Service;

import java.util.List;


public class MovieSearchFragment extends MovieBaseFragment {


    RecyclerView recyclerView;
    MovieAdapter movieAdapter;
    Service restRequest;
    BaseCallback baseCallback;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFragment(view);
        initRecyclerView(view);
        restRequest.searchByTitle(getArguments().getString("query")).enqueue(baseCallback);
    }

    private void initFragment(View view){
        RequestManager.getInstance().initialize();
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        restRequest = RequestManager.getInstance().getService();
        movieAdapter = new MovieAdapter(getContext());
        baseCallback = new BaseCallback() {
            @Override
            public void onSuccess(List<Movie> list) {
                movieAdapter.Pagination(list);
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void initRecyclerView(View view){
        GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(),1);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(movieAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(view.getContext(), new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                Movie movie = movieAdapter.getItem(position);
                startActivity(new Intent(view.getContext(), Movie_Detail.class).putExtra(Parc.EXTRA_MOVIE_PARCELABLE, movie));
            }

        }));
    }
}
